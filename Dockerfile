FROM php:7.0-apache

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        git \
        curl \
    && docker-php-ext-install -j$(nproc) iconv mcrypt mbstring zip \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd


RUN curl -sL https://deb.nodesource.com/setup_4.x | bash -
RUN apt-get install -yq nodejs build-essential


RUN npm install -g npm
RUN npm install --global gulp
RUN npm install --global gulp-cli


ENV PATH=/root/.composer/vendor/bin:$PATH

RUN composer global require "laravel/installer"
