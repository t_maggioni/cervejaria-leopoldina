## Instalar o projeto

Clone o repositório

```
$ git clone git@bitbucket.org:organicadigital/hefesto.git
$ cd hefesto
```

## Provisionando

1. O primeiro passo é você ter o **Docker** na sua máquina. Para fazer download, acese [essa página](https://docs.docker.com/engine/installation/).

2. Caso o **Docker** já esteja instalado execute o comando:

  ```
  $ docker-compose up -d
  ```

3. Configurar o site pelo arquivo .env
Renomeie o arquivo .env.example para .env
Inseria a chave do site321 na variável "SITE321_TOKEN";
Na variável "APP_KEY", rode o comando abaixo para conseguir a chave.

```
$ docker-compose exec web php artisan key:generate
```

4. Execute o comando abaixo para installar as dependencias

  ```
  $ docker-compose exec web composer install
  ```

## Cria projeto do zero (por padrão não é necessário)

```
$ docker-compose exec web lumen new nome-do-projeto
$ docker-compose exec web composer install
```

## Rodar o php

```
$ docker-compose exec web php -S 0.0.0.0:8000 -t public
```

## Instalar novas dependencias

```
$ docker-compose exec web composer install
```

## Criar ambiente para realizar o deploy
```
# -------------------------------------------------------
# LOCAL
# -------------------------------------------------------
# Crie uma cópia do arquivo create_gitbare.example.sh com o nome create_gitbare.sh e coloque as informações do servidor solicitadas no arquivo, depois rode o comando abaixo.
$ sh create_gitbare.sh
# Quando executado o comanda acima exibira uma mensagem solicitando que execute o seguite comando "git remote add production ssh://[usuário]@[ip]/home/[repositorio do usuario]/repository"
# Execute
$ git push production master

# -------------------------------------------------------
# SERVIDOR
# -------------------------------------------------------
# Adicione as 2 extensões no arquivo phprc, na Dreamhost fica em /home/[usuario]/.php/[versao-do-php-que-esta-utilizando]/phprc
$ extension=phar.so
$ extension=fileinfo.so
# Vá até o root do projeto ( /home/[usuario]/[projeto]) e rode os comandos abaixo:
$ mkdir -p storage/framework/cache/data && chmod 777 storage/framework/cache/data
$ mkdir -p public/uploads && chmod 777 public/uploads
$ php -d extension=phar.so -d extension=fileinfo.so composer.phar install --no-scripts --no-dev --no-interaction --prefer-dist
$ cp .env.example .env
# Rode o comando abaixo e copie a chave, ele será inserido no arquivo .env
$ php artisan key:generate
# Edite o .env e coloque as informação solicitadas no arquivo ( não esquecer de alterar o APP_ENV=local para APP_ENV=production, colocar a chave gerada no comando acima e o TOKEN do site321)
$ vim .env
# Execute
$ php artisan config:clear
$ php artisan cache:clear
# Execute a etapa de otimização a aplicação.
```

## Otimizar aplicação (Somente em produção)
```
$ php -d extension=phar.so -d extension=fileinfo.so composer.phar dump-autoload
$ php artisan clear-compiled
$ php artisan config:cache
$ php artisan route:cache
$ php artisan optimize --force

# Coloquei todos os comandos acima em uma unica linha
$ php -d extension=phar.so -d extension=fileinfo.so composer.phar dump-autoload && php artisan clear-compiled && php artisan config:cache && php artisan route:cache && php artisan optimize --force
```

## Adicionar no git o endereço do servidor

```
$ git remote add production ssh://[usuário]@[ip]/home/[repositorio do usuario]/repository
```


## Fazer deploy

```
$ git push production master
```
Observação: para fazer o deploy é necessário ter adicionado o endereço do servidor no git.


## Rodar MIX
```
# Instalar dependências
$ npm install

# Quando realizar ajustes nos assets rode.
$ npm run watch

# Gerar os assets
$ npm run dev

```

## Obersações em gerais

* As imagens geradas e guardadas em cache ficam public/uploads
