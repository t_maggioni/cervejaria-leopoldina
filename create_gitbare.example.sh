# remgit.sh
# Creates a remote git repository from the current local directory

# Configuration
# Replace SSH_USERNAME, SSH_HOST, SSH_GIT_PATH with your details
USER="Usuário do ssh"
HOST="Endereço do ssh"
GIT_PATH="Caminho no servidor que ficará o site"
PATH_REPO="Caminho no servidor que será criado o controle do git"

GIT_REMOTE_URL=ssh://$USER@$HOST/$PATH_REPO

echo "-------------------------------------------"
echo "------ Building New Git Repository --------"
echo "-------------------------------------------"

# Setup remote repo

echo "--"
echo "-- Creating bare remote repo at:"
echo "-- $USER@$HOST/$GIT_PATH/$PATH_REPO"
echo "--"


# Tem que ainda adicionar as extencoes (phar.so e fileinfo.so) no arquivo /home/cafegramado/.php/7.0/phprc
#extension=phar.so
#extension=fileinfo.so

ssh $USER@$HOST 'mkdir '$PATH_REPO' && cd '$PATH_REPO' && git --bare init && git --bare update-server-info && touch hooks/post-receive && echo -e "#!/bin/sh\ngit --work-tree='$GIT_PATH' --git-dir='$PATH_REPO' checkout -f" >> hooks/post-receive && chmod a+x hooks/post-receive && cp hooks/post-update.sample hooks/post-update && echo -e "echo \"- Acessando diretório\"
cd '$GIT_PATH'
echo \"- Executando  composer\"
exec php -d extension=phar.so -d extension=fileinfo.so ./composer.phar install --no-scripts --no-dev --no-interaction --prefer-dist
echo \"- Concluido composer\"" >> hooks/post-update && chmod a+x hooks/post-update && echo "alias \"php=/usr/local/php70/bin/php\"" >> ~/.bash_profile && echo "export PATH=/usr/local/php70/bin:$PATH" >> ~/.bash_profile && touch git-daemon-export-ok'


echo "--"
echo "-- Your new git repo '$PATH_REPO' is ready and initialized at:"
echo "-- git remote add production $GIT_REMOTE_URL"
echo "--"
