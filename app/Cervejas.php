<?php

namespace App;

use \Site321;

class Cervejas extends Site321
{

  public $path = '/contents/cerveja';

  function findBy($id){
    return $this->find($id)['item'];
  }
}
