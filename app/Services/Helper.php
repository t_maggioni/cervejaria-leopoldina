<?php

namespace App\Services;

use \Illuminate\Support\Str;
use \Cache;
use \Image;

class Helper
{

  static function autolink_phone($string){
    $regex = '/(\([0-9]{2}\)\s[0-9]{4,4}[-. ]?[0-9]{4,4})/i';
    $replace = '<a href="tel:${1}">${1}</a>';
    $string = preg_replace($regex, $replace, $string);
    $regex = '/tel:\(([0-9]{2})\)\s([0-9]{4,4})[-. ]?([0-9]{4,4})/i';
    $replace = 'tel:${1}${2}${3}';

    return preg_replace($regex, $replace, $string);
  }

  static function truncate($str, $limit)
  {
    return Str::limit($str, $limit);
  }

  static function image_url($img, $size = null, $alt = null)
  {
    $key = explode('img-', $img)[1];

    $url = '/img/'.$key;
    $url .= (!empty($alt)) ? '/'.str_slug($alt) : '';
    $url .= (!empty($size)) ? '?thumb='.$size : '';

    
    return $url;
  }

}
