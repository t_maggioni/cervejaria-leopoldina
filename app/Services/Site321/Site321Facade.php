<?php

namespace App\Services\Site321;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Collection;

class Site321Facade extends Facade
{

    protected $path;
    protected $params;
    protected $collection;
    protected $has_many = null; // Array
    protected $belongs_to = null; // Array
    protected $order_by = null; // Array [field, desc|asc]

    protected static function getFacadeAccessor()
    {
        return 'site321';
    }


    public function setTable($path)
    {
      $this->path = $path;
    }

    public function children_menu($id)
    {
      $menu = $this->collection->reject(function ($links) use ($id) {
        return ($id != $links['parent_id']);
      });

      if (count($menu) > 0){
        return $menu->values()->map(function ($links) {

            $id = $links['page'];
            if(!empty($links['page_template']))
            {
              $request = $this->request('/contents/'.$links['page_template'].'/'.$id, 'find-'.$id);

              if(isset($request['item']['slug']))
              {
                $links['url'] = '/'.$request['item']['slug'];
              }
            }
            $links['children'] = $this->children_menu($links['id']);
            return $links;

        });
      }

    }

    public function recursive_search($collection = null)
    {
      if (!empty($collection))
      {
        $this->collection = $collection;
      }

      return $this->collection->map(function ($links) {

        if(empty($links['parent_id'])){
          $id = $links['page'];
          if(!empty($links['page_template']))
          {
            $request = $this->request('/contents/'.$links['page_template'].'/'.$id, 'find-'.$id);
            if(isset($request['item']['slug']))
            {
              $links['url'] = '/'.$request['item']['slug'];
            } elseif(isset($request['item']['title'])) {
              $links['url'] = '/'. str_slug($request['item']['title']);
            } elseif(isset($request['item']['name'])) {
              $links['url'] = '/'. str_slug($request['item']['name']);
            }
          }

          $links['children'] = $this->children_menu($links['id']);
          
          return $links;
        }

      });
      

    }

    public function menu($menu = 'principal')
    {
      $menu = '/menus/'.$menu;
      $key_cache = $this->increment_key($menu);

      if (Cache::has($key_cache))
      {
        return Cache::get($key_cache);
      } else {
        $this->collection = collect($this->response($menu));
        $result = $this->recursive_search()->reject(function ($value, $key) {
            return $value == null;
        })->values()->all();
        if($result != null) Cache::forever($key_cache, $result);
        return $result;
      }
    }

    public function all(array $options = [])
    {

      if(isset($options['params']))
      {
        $this->setParams($options['params']);
      }

      return $this->request($this->path, 'all');
    }

    public function where(array $attrs = [])
    {
      $this->setParams($attrs, true);

      return $this->request($this->path, 'where');
    }

    public function find($id)
    {
      return $this->request($this->path.'/'.$id, 'find-'.$id);
    }

    public function save($key, $params = []){
      $response = \Guzzle::post(
                        '/api/v2/contents/'. $key,
                        [ "form_params" => $params]);

      return (string) $response->getBody();
    }

    public function setParams($attrs, $search = false)
    {
      $params_arr = [];
      $expect = ['per', 'page'];
      foreach($attrs as $attr_key => $attr_value )
      {
          if($search == true){
            if(in_array($attr_key, $expect)){
              $params_arr[] = $attr_key .'='. $attr_value;
            } else {
              $params_arr[] = 'search['.$attr_key .']='. $attr_value;
            }
          } else {
            $params_arr[] = $attr_key .'='. $attr_value;
          }
      }
      $this->params = '?' . implode($params_arr, '&');
    }

    public function increment_key($key = '')
    {
      $replace = ['/','&','=', '?', '[', ']'];
      return substr(
          str_replace($replace, '-', $key.$this->params)
        , 1);
    }

    public function foreach_has_many($value)
    {
      foreach($this->has_many as $key => $attr )
      {
        list($row, $path, $field) = explode('/', $attr[0]);
        $this->params = '';

        $this->setParams([$attr[1] => $value['id'], 'per' => 100], true);
        $result_has = $this->response($attr[0]);
        $value[$field] = $result_has;
      }
      return $value;
    }

    public function foreach_belongs_to($value)
    {
      foreach($this->belongs_to as $key => $attr )
      {
        list($row, $path, $field) = explode('/', $attr[0]);

        if(isset($value[$attr[1]]) && count($value[$attr[1]])>0)
        {
          $params_arr = [];
          foreach($value[$attr[1]] as $id) { $params_arr[] = 'search[id][]=' . $id; }
          $this->params = '?' . implode($params_arr, '&');
          $result_has = $this->response($attr[0]);
          $value[$attr[1].'_items'] = $result_has['items'];
        }
      }
      return $value;
    }

    public function mount_association($result, $type_association)
    {

      if(isset($result['items']))
      {
        return collect($result['items'])->map(function($value) use ($type_association)
        {
          if ($type_association == 'belongs_to') {
            return $this->foreach_belongs_to($value);
          } else {
            return $this->foreach_has_many($value);
          }
        });
      } else {
        if ($type_association == 'belongs_to') {
          return $this->foreach_belongs_to($result);
        } else {
          return $this->foreach_has_many($result);
        }
      }
    }

    public function request($path, $increment = '')
    {
      $key_cache = $this->increment_key($path.$increment);

      if (Cache::has($key_cache))
      {
        return Cache::get($key_cache);
      } else {
        $result = $this->response($path);
        if(!empty($this->has_many))
        {
          $result = $this->mount_association($result, 'has_many');
        }

        if(!empty($this->belongs_to))
        {
          $result = $this->mount_association($result, 'belongs_to');
        }

        if(!empty($this->order_by))
        {
          if( $this->order_by[1] == 'asc' ){
            $result = collect($result)->sortBy(function ($res, $key) {
              return $res[$this->order_by[0]];
            });
          } else {
            $result = collect($result)->sortByDesc(function ($res, $key) {
              return $res[$this->order_by[0]];
            });
          }
        }

        if($result != null) Cache::forever($key_cache, $result);
        return $result;
      }
    }

    public function response($path)
    {
      try {
        $response = \Guzzle::get('/api/v2/'.config('app.site321').$path.$this->params);
        return json_decode((string) $response->getBody(), true);
      } catch (\GuzzleHttp\Exception\ClientException $e) {
        return null;
      }
    }

}
