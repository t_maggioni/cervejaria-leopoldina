<?php

namespace App\Services\Site321;

use Illuminate\Support\Facades\Facade;
// use Illuminate\Support\Facades\Cache;
// use Illuminate\Support\Collection;


class Site321Helper extends Facade
{

  protected static function getFacadeAccessor()
  {
      return 'site321_helper';
  }

  public static function link_to()
  {
      return 'OAL';
  }
}
