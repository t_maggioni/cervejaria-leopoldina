<?php

namespace App;

use \Site321;

class Janelas extends Site321
{

  public $path = '/contents/janela';

  function findBy($id){
    return $this->find($id)['item'];
  }

}
