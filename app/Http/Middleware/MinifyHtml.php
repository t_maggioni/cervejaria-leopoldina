<?php

namespace App\Http\Middleware;

use Closure;
use Cache;
use Illuminate\Support\Str;
// use Illuminate\Http\Response;


class MinifyHtml
{

    protected $except = [ 'flush_cache' ];


    public function handle($request, Closure $next)
    {

        $key = $this->keygen($request->getRequestUri());

        if(\Cache::has($key)){
            return response(\Cache::get($key));
        }
        return $next($request);
    }


    public function terminate($request, $next){

      if(!in_array($request->path(), $this->except) && !app()->isLocal()){
        $key = $this->keygen($request->getRequestUri());
        if (!\Cache::has($key))
        {
          $output = $this->minifier($next->getContent());
          \Cache::forever($key,$output);
        }
      }
      return $next;

    }

    protected function keygen($url)
    {
       return 'html_' . str_slug($url);
    }

    public function minifier(string $html)
    {
      $htmlFilters = [
        '/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s' => '',
        '/(?<!\S)\/\/\s*[^\r\n]*/' => '',
        '/\s{2,}/' => ' ',
        '/>\s+</' => '><',
        '/(\r?\n)/' => '',
      ];
      return preg_replace(array_keys($htmlFilters), array_values($htmlFilters), $html);
    }

}
