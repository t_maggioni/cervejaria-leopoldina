<?php

namespace App\Http\Middleware;

use Closure;
use Cache;
use Image;
use Illuminate\Support\Str;

class ImageCache
{

    protected $except = [ 'flush_cache' ];

    public function handle($request, Closure $next)
    {
      $path_upload = public_path().'/uploads/';
      $key = $this->keygen($request->getRequestUri());

      $regex = '/\/img\/([0-9a-z]+)[\?\/]?(.*)/i';
      $imageId = preg_replace($regex, '${1}', $request->getRequestUri());

      if(!file_exists($path_upload.$key)){

        $params = '';
        if($request->has('thumb'))
        {
          $params .= '?thumb='.urlencode($request->query('thumb'));
        }

        $img = Image::make('https://www.thumb321.com.br/img-'.$imageId.$params);
        $img->save($path_upload.$key, 70);

      }
      return $next($request);
    }

    protected function keygen($url)
    {
       return str_slug($url);
    }

}
