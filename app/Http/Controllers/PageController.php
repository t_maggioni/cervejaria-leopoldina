<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Site321Helper;

use App\Home;
use App\Cervejas;
use App\PagCervejas;
use App\PagKits;
use App\Kits;
use App\Elementos;
use App\PagInstitucional;
use App\PagFabrica;
use App\PagContato;
use App\Contato;
use App\Conteudo;
use App\Galeria;




// --- Iniciar
// $Categoria = new Categoria();
// --- Submeter dados para o site321
// return $Categoria::post("2a0bc7909598af28", ["name" => 'Mailon', "email" => 'mailon@organicadigitla.com', "message" => 'Inserindo via api']);
// --- Consulta com paginação
// echo $Categoria::all(["params" => ["per" => 2, "page"=> 2]]);
// --- Buscar um registro especifico
// $find = $Categoria->find('f8ca03ae-5e6a-4883-b49e-f0fa2292f054');
// --- Buscar tudo
// return $find->all('item');
// --- Fazer uma pesquisa
// return Cardapio::where([ "title"=> "La Minuta" ]);


class PageController extends Controller
{

    function index()
    {
      $data = new Home();
      $cervejas = new Cervejas();
      $passvar = $this->passvar(['home' => true, 'data' => $data->all(), 'cervejas' => $cervejas->where([ "destaque_home"=> "true" ])]);
	    return view('index',$passvar);
    
    }
    function cervejas(){

      $data = new PagCervejas();
      $cervejas = new Cervejas();
      $passvar = $this->passvar(['data' => $data->all(), 'cervejas' => $cervejas->all() ]);

      return view('cervejas', $passvar);
    }


  function detalhe_cerveja($slug){
  
      $data = new Cervejas();
      $cervejas = new Cervejas();
      $cervejas2 = new Cervejas();
      $elementos = new Elementos();
      $cervejaID =  $cervejas2->where([ "slug"=> $slug ]);
      $cervejaID = $cervejaID['items'][0]['id'];
      

      $passvar = $this->passvar(['data' => $data->findBy($cervejaID), 'cervejas' => $cervejas->all(), 'cerva' => $cervejas->where([ "slug"=> $slug ]), 'elementos' => $elementos->where(["cerveja" => $cervejaID])]);

      return view('cerva', $passvar);
    }

    function kits(){
      $data = new PagKits();
      $kits = new Kits();
      $passvar = $this->passvar(['data' => $data->all(), 'kits' => $kits->all() ]);

      return view('kits', $passvar);
    }

    function institucional(){

      $data = new PagInstitucional();
      $passvar = $this->passvar(['data' => $data->findBy()]);

      return view('institucional', $passvar);
    }

    function fabrica(){

      $data = new PagFabrica();
      $passvar = $this->passvar(['data' => $data->findBy()]);

      return view('fabrica', $passvar);
    }

    function contato(){
      $data = new PagContato();
      $passvar = $this->passvar(['data' => $data->all()]);

      return view('contato', $passvar);
    }

    function conteudo($slug){
      $data = new Conteudo();
      $data =  $data->where([ "slug"=> $slug]);
      $data = $data['items'][0];
      
      $passvar = $this->passvar(['data' => $data]);

      return view('conteudo', $passvar);
    }

    function galeria($slug){
      $data = new Galeria();
      $data =  $data->where([ "slug"=> $slug]);
      $data = $data['items'][0];
      
      $passvar = $this->passvar(['data' => $data]);

      return view('galeria', $passvar);
    }
    

    function busca(Request $request){

      $termo = $request->input('search');

      //busca cervejas
      $data = new PagCervejas();
      $cervejas = new Cervejas();
      $cervejas =  $cervejas->all();
      $cervejas =  $cervejas['items'];
      $data = $data->all();
      $data['title'] = "Busca por " .$termo;
      
      $cervejas = collect($cervejas);
      $cervejas = $cervejas->filter(function($item) use ($termo){
          return (false !== stristr($item['title'], $termo) || false !== stristr($item['descricao'], $termo));
      });
     

        // busca kits
      $kits = new Kits();
      $kits =  $kits->all();
      $kits =  $kits['items'];

      $kits = collect($kits);
      $kits = $kits->filter(function($item) use ($termo){

        return (false !== stristr($item['title'], $termo) || false !== stristr($item['description'], $termo));
      });



      $passvar = $this->passvar(['data' => $data, 'cervejas' => $cervejas,'termo' => $termo, 'kits' => $kits]);
       
      
      return view('busca', $passvar);
      
    }

                      
}
