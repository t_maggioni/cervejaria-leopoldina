<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Config;
use App\Tema;
use App\Menu;
use App\Janelas;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    static function passvar($pass = [])
    {

      $Menu = new Menu();
      $Config = new Config();
      $Tema = new Tema();
      $Janela = new Janelas();

      $var_default = [
        'menu' => $Menu->menu(),
        'config' => $Config->all(),
        'tema' => $Tema->all(),
        'janelaNews' => $Janela->findBy("4b7013d6-bfb4-4b6b-abb0-27a4169bccf4"),
        'janelaFicha' => $Janela->findBy("ee712ed4-d334-4f42-8184-0ed749d16b0b"),
      ];

      return array_merge($var_default, $pass);
    }

}
