<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// ----- ROTAS DAS PÁGINAS
Route::get('/', 'PageController@index')->middleware('html_cache');



/*
|--------------------------------------------------------------------------
| ATENÇÃO: AS ROTAS ABAIXO NÃO DEVEM SER REMOVIDAS
|--------------------------------------------------------------------------
|
| As rotas abaixo são requisitos básicos para um bom funcionamento, não remova!
| caso ache necessário alterar, altere, mas tome o máximo de cuidado
|
*/

// ----- LIMPAR O CACHE DA API E IMAGENS
Route::get('/flush_cache', function(){
  Cache::flush();

  $photos = File::allFiles('uploads/');
  foreach ($photos as $photo) { File::delete($photo); }
  \Guzzle::post('http://beta.site321.com.br/api/v1/'.config('app.site321'). '/update_completed');

  return 'OK';

});

// ----- URL QUE GERA E BUSCA AS IMAGENS CACHEADAS
Route::get('/img/{id}/{slug?}', function($imageId, $slug = null)
{
  $path = 'uploads/'.str_slug(request()->getRequestUri());
  return response()->file($path);

})->middleware('image_cache');

// ----- GERA O SITEMAP
Route::get('sitemap.xml', function()
{
  $sitemap = App::make("sitemap");
  $sitemap->setCache('sitemap');

  if( !$sitemap->isCached() )
  {
      $time = Carbon\Carbon::yesterday();
      $sitemap->add( URL::to('/'), $time, '1.0', 'monthly' );

      $Menu = new App\Menu();
      $menu = $Menu->menu();
      foreach ($menu as $key => $value)
      {
        if( !empty($value['page_template']) )
        {
          $sitemap->add(URL::to($value['url']), $time, '0.9', 'monthly');
        }
      }
  }
  return $sitemap->render();
});

Route::get('/inicial', 'PageController@index')->middleware('html_cache');
Route::get('/cervejas', 'PageController@cervejas')->middleware('html_cache');
Route::get('/cervejas/{slug}','PageController@detalhe_cerveja')->middleware('html_cache');
Route::get('/kits', 'PageController@kits')->middleware('html_cache');
Route::get('/a-cervejaria-leopoldina', 'PageController@institucional')->middleware('html_cache');
Route::get('/fabrica', 'PageController@fabrica')->middleware('html_cache');
Route::get('/contato', 'PageController@contato')->middleware('html_cache');
Route::get('/conteudo/{slug}', 'PageController@conteudo')->middleware('html_cache');
Route::get('/galeria/{slug}', 'PageController@galeria')->middleware('html_cache');
Route::post('/busca/', 'PageController@busca')->middleware('html_cache');


