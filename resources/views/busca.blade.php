@extends('layouts.app')

@section('header')
@include('partials.header_pages')
@endsection
@section('content')
@php
  if(count($cervejas) > 0 || count($kits) > 0){
  @endphp
  @foreach ($cervejas as $cerveja)
  <div class="section-beer">
    <div class="container">
      <div class="w-row">
        <div class="w-col w-col-3">
          <!--<img data-ix="bottomanimation-1" src="{{ $cerveja['foto_da_garrafa'] }}" width="230">-->
         <img data-ix="bottomanimation-1" src="{{ Helper::image_url($cerveja['foto_da_garrafa'], '230x') }}" width="230">
          
        </div>
        <div class="w-col w-col-9">
          <div class="section-beer_title-line" style="background-color:{{ $cerveja['cor']['value'] }}"></div>
          <h2 class="seal-title section-first-beer-title section-title" data-ix="bottomanimation-2">{{ $cerveja['title'] }}</h2>
          <div class="w-row">
            <div class="w-col w-col-2">
              <img class="section-beer-seal" data-ix="bottomanimation-2" src="{{ $cerveja['selo_principal'] }}" width="134">
            </div>
            <div class="w-col w-col-10">
              <div class="seal-paragraph section-first-beer-paragraph section-paragraph" data-ix="bottomanimation-2">{!! $cerveja['descricao'] !!}</div>
            </div>
          </div>
          <div class="section-beer-buttons">
            <a class="btn btn_style_2 section-products_home-btn w-button" href="/cervejas/{{$cerveja['slug']}}">{{ $data['btn_acesso'] }}</a>
            <a class="btn w-button" href="https://loja.famigliavalduga.com.br/Produtos/produtor/cervejaria-leopoldina" target="_blank">{{ $data['btn_compra'] }}</a>
          </div>
        </div>
      </div>
    </div>
  </div>      
  @endforeach

  @foreach ($kits as $kit)

    <div class="section-beer section-kit">
    <div class="container">
      <div class="w-row">
        <div class="w-col w-col-6">
          <img data-ix="bottomanimation-1" sizes="(max-width: 767px) 90vw, (max-width: 991px) 46vw, 444.5px" src="{{$kit['photo']}}" srcset="{{$kit['photo']}}?thumb=500x 500w, {{$kit['photo']}}?thumb=889x 889w" width="444.5">
        </div>
        <div class="w-col w-col-6">
          <h2 class="kit-title section-first-beer-title section-title" data-ix="bottomanimation-2">{{ $kit['title'] }}</h2>
          <p class="kit-paragraph section-first-beer-paragraph section-paragraph" data-ix="bottomanimation-2">{!! $kit['description'] !!}</p>
          <div class="section-beer-buttons">
            <a class="btn w-button" href="{{$kit['btn_acao_url']}}">{{$kit['btn_acao_texto']}}</a>
          </div>
        </div>
      </div>
    </div>
  </div>    
  @endforeach
@php
}else{ @endphp
<div class="paragraph-section">
      <div class="container">
    Nenhum registro encontrado com o termo {{$termo}}
    </div>
</div>
@php
  
}
@endphp
  
  
@endsection