@extends('layouts.app')

@section('header')
@include('partials.header_pages')
@endsection
@section('content')
<div class="image-section" style="background-image:url({{$data['background_img']}})"></div>
  <div class="paragraph-section">
    <div class="container">
      {!! $data['conteudo'] !!}
     
    </div>
  </div>
  <div class="image-section" style="background-image:url({{$data['background_img']}})"></div>
@endsection