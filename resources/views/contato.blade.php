@extends('layouts.app')

@section('header')
@include('partials.header_pages')
@endsection
@section('content')
  <div class="section-contact-form">
    <div class="container">
      <div class="section-paragraph" data-ix="bottomanimation-2">{!!$data['conteudo']!!}</div>
      <div class="w-form">
        <form class="w-clearfix form-ajax" action="http://beta.site321.com.br/api/v2/contents/4d712c991d504bce"  data-name="Email Form 2" id="email-form-2" name="email-form-2">
          <div class="w-row">
            <div class="w-col w-col-4" data-ix="bottomanimation-1">
              <label class="form-label" for="name-2">Nome:</label>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input class="form-input w-input" data-name="name" id="name-2" maxlength="256" name="name" placeholder="Digite seu nome" required="required" type="text">
            </div>
            <div class="w-col w-col-4" data-ix="bottomanimation-2">
              <label class="form-label" for="phone">Telefone:</label>
              <input class="form-input w-input" data-name="phone" id="phone" maxlength="256" name="phone" placeholder="Telefone para contato" required="required" type="text">
            </div>
            <div class="w-col w-col-4" data-ix="bottomanimation-2">
              <label class="form-label" for="email-3">E-mail:</label>
              <input class="form-input w-input" data-name="email" id="email-3" maxlength="256" name="email" placeholder="Seu e-mail principal" required="required" type="email">
            </div>
          </div>
          <div class="form-text-area-box">
            <label class="form-label" for="message">Mensagem:</label>
            <textarea class="form-textarea w-input" data-name="message" id="message" maxlength="5000" name="message" placeholder="Digite sua mensagem..." required="required"></textarea>
          </div>
          <input class="btn form-button w-button" data-wait="Please wait..." type="submit" value="Enviar mensagem">
        </form>
        <div class="w-form-done">
          <div>Thank you! Your submission has been received!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Something went wrong while submitting the form</div>
        </div>
      </div>
    </div>
  </div>
 @endsection 