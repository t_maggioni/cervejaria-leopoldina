@extends('layouts.app')

@section('header')
@include('partials.header_pages')
@endsection
@section('content')
 
  @foreach ($kits['items'] as $kit)

    <div class="section-beer section-kit">
    <div class="container">
      <div class="w-row">
        <div class="w-col w-col-6">
          <img data-ix="bottomanimation-1" src="{{ Helper::image_url($kit['photo'], '444x') }}"  width="444.5">
        </div>
        <div class="w-col w-col-6">
          <h2 class="kit-title section-first-beer-title section-title" data-ix="bottomanimation-2">{{ $kit['title'] }}</h2>
          <p class="kit-paragraph section-first-beer-paragraph section-paragraph" data-ix="bottomanimation-2">{!! $kit['description'] !!}</p>
          <div class="section-beer-buttons">
            <a class="btn w-button" href="{{$kit['btn_acao_url']}}">{{$kit['btn_acao_texto']}}</a>
          </div>
        </div>
      </div>
    </div>
  </div>    
  @endforeach
  
  
@endsection