@extends('layouts.app')

@section('header')
@include('partials.header_home')
@endsection
@section('content')
  <section class="section-beer-backend">
    <div class="container">
      <div class="w-row">
        <div class="beer-background-video-column w-col w-col-3" data-ix="bottomanimation-1">
          <div class="beer-background-video w-background-video" data-autoplay="data-autoplay" data-loop="data-loop" data-poster-url="https://daks2k3a4ib2z.cloudfront.net/58bd6958ab924a5c02bd66c6/58f5230f27dc5e3571d8ce73_beer_background-poster-00001.jpg" data-video-urls="{{ URL::asset('videos/beer_background-transcode.webm,videos/beer_background-transcode.mp4') }}" data-wf-ignore="data-wf-ignore">
            <img class="beer-background-image-video" src="{{ URL::asset('images/moldura_beer-hidpi.png') }}" width="209">
          </div>
        </div>
        <div class="w-col w-col-9">
          <h2 class="beer-backend-section-title section-title" data-ix="bottomanimation-2">{{ $data['title_section_2'] }}</h2>
          <p class="beer-backend-paragraph section-paragraph" data-ix="bottomanimation-2">{{ $data['description_section_2'] }}</p>
          <div class="background-beer-detail">
            <div class="background-beer-detail-line" data-ix="bottomanimation-2"></div>
            <div class="background-beer-detail-text">
              <h3 class="background-beer-detail-title" data-ix="bottomanimation-2">{{ $data['elemento_1_title'] }}</h3>
              <p class="background-beer-detail-paragraph" data-ix="bottomanimation-2">{{ $data['elemento_1_text'] }}</p>
            </div>
          </div>
          <div class="background-beer-detail">
            <div class="background-beer-detail-line" data-ix="bottomanimation-2"></div>
            <div class="background-beer-detail-text">
              <h3 class="background-beer-detail-title" data-ix="bottomanimation-2">{{ $data['elemento_2_title'] }}</h3>
              <p class="background-beer-detail-paragraph" data-ix="bottomanimation-2">{{ $data['elemento_2_text'] }}</p>
            </div>
          </div>
          <div class="background-beer-detail background-beer-detail-2">
            <div class="background-beer-detail-line" data-ix="bottomanimation-2"></div>
            <div class="background-beer-detail-text">
              <h3 class="background-beer-detail-title" data-ix="bottomanimation-2">{{ $data['elemento_3_title'] }}</h3>
              <p class="background-beer-detail-paragraph" data-ix="bottomanimation-2">{{ $data['elemento_3_text'] }}</p>
            </div>
          </div>
          <div class="background-beer-detail background-beer-detail-2">
            <div class="background-beer-detail-line" data-ix="bottomanimation-2"></div>
            <div class="background-beer-detail-text">
              <h3 class="background-beer-detail-title" data-ix="bottomanimation-2">{{ $data['elemento_4_title'] }}</h3>
              <p class="background-beer-detail-paragraph" data-ix="bottomanimation-2">{{ $data['elemento_4_text'] }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section-products_home">
    <div class="container">
      <div class="w-row">
         @foreach ($cervejas['items'] as $cerveja)
        <div class="w-col w-col-4">
          <div class="section-products_home-item">
            <!--<img height="490" src="{{ $cerveja['foto_da_garrafa'] }}">-->
            <img height="490" src="{{ Helper::image_url($cerveja['foto_da_garrafa'], '163x') }}">
            
            <h2 class="section-products_home-title">{{ $cerveja['title'] }}</h2>
            <a class="btn btn_style_2 section-products_home-btn w-button" href="/cervejas/{{$cerveja['slug'] }}">{{ $data['btn_acesso_cerveja'] }}</a>
            <a class="btn w-button" href="https://loja.famigliavalduga.com.br/Produtos/produtor/cervejaria-leopoldina" target="_blank">{{ $data['btn_compra_cerveja'] }}</a>
          </div>
        </div>
          @endforeach
        <!--<div class="w-col w-col-4">
          <div class="section-products_home-item">
            <img height="490" src="{{ URL::asset('images/ipa_hd.png') }}">
            <h2 class="section-products_home-title">Leopoldina IPA</h2>
            <a class="btn btn_style_2 section-products_home-btn w-button" href="leopoldina-witbier.html">Conheça</a>
            <a class="btn w-button" href="https://loja.famigliavalduga.com.br/Produtos/produtor/cervejaria-leopoldina" target="_blank">Comprar online</a>
          </div>
        </div>
        <div class="w-col w-col-4">
          <div class="section-products_home-item">
            <img height="490" src="{{ URL::asset('images/weissbier_hd.png') }}">
            <h2 class="section-products_home-title">Leopoldina Weissbier</h2>
            <a class="btn btn_style_2 section-products_home-btn w-button" href="leopoldina-witbier.html">Conheça</a>
            <a class="btn w-button" href="https://loja.famigliavalduga.com.br/Produtos/produtor/cervejaria-leopoldina" target="_blank">Comprar online</a>
          </div>
        </div>-->
      </div>
    </div>
  </section>
@endsection
