@if(!empty($config['ga_token']))
<script>
var galite = galite || {};
galite.UA = '{{ $config['ga_token'] }}';
</script>
@endif
@if(!empty($config['tag_manager_token']))
<script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer',"{{ $config['tag_manager_token'] }}");
</script>
@endif