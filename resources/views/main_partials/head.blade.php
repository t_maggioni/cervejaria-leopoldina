<head>
  <meta charset="utf-8">
 <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="index,follow" name="robots">
  <link href="{{ URL::asset('css/normalize.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ URL::asset('css/components.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ URL::asset('css/cervejaria-leopoldina-v2.css') }}" rel="stylesheet" type="text/css">


  <style>
  .search-modal_input::-webkit-input-placeholder { /* Chrome/Opera/Safari */
    color: white;
  }
  .search-modal_input::-moz-placeholder { /* Firefox 19+ */
    color: white;
  }
  .search-modal_input:-ms-input-placeholder { /* IE 10+ */
    color: white;
  }
  .search-modal_input:-moz-placeholder { /* Firefox 18- */
    color: white;
  }
</style>
  @include('main_partials.meta_tag')
  @include('main_partials.tracking')
  {{ $config['html_head'] }}
</head>
