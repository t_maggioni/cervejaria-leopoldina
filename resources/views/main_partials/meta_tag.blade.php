@php
  $title = '';
  if(!empty($data['title'])){ $title = $data['title'] . ', '; }
  $title .= $tema['site_title'];
  $title = Helper::truncate($title, 60);

  $description = $tema['meta_description'];
  if(isset($data['meta_description'])){ $description = $data['meta_description']; }
  $description = Helper::truncate($description, 150);

  $og_image = $tema['og_image'];
  if(isset($data['og_image'])){ $og_image = $data['og_image']; }
@endphp
<title>{{ $title }}</title>
<meta content="{{ $description }}" name="description">
<meta content="{{ $title }}" property="og:title">
<meta content="{{ $description }}" property="og:description">
<meta content="{{ $og_image }}?thumb=600x315%23" property="og:image">
<meta content="{{ Request::url() }}" property="og:url">
<link href="{{ Request::url() }}" rel="canonical">
<link href="{{ $tema['favicon'] }}" rel="shortcut icon" type="image/x-icon">
<link href="{{ $tema['logo'] }}" rel="apple-touch-icon">
