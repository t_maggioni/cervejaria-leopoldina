<div class="search-modal">
    <div class="container">
      <div class="w-form">
        <form data-name="Email Form 2" id="email-form-2" name="email-form-2" action="busca" method="post">
          <input class="search-modal_input w-input" data-name="search" id="search" maxlength="256" name="search" placeholder="O que você está procurando?" type="text">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input class="search-modal_button w-button" data-wait="Please wait..." type="submit" value="Buscar">
        </form>
        <div class="w-form-done">
          <div>Obrigado! Você se cadastrou para receber novidades!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Algo deu errado</div>
        </div>
      </div>
    </div>
    <a class="navbar-close-button w-inline-block" data-ix="closesearchmodal" href="#"></a>
  </div>
  <div class="download-modal">
    <div class="container">
      <h2 class="download-modal_title">{{$janelaFicha['title']}}</h2>
      <p class="modal-download_paragraph">{!!$janelaFicha['description']!!}</p>
      <div class="w-form">
        <form class="download-modal_form w-clearfix form-ajax" action="http://beta.site321.com.br/api/v2/contents/33233b596d326149" data-name="Email Form 4" id="email-form-4" name="email-form-4">
          <div class="w-row">
            <div class="w-col w-col-6">
              <label class="download-modal_form-label" for="name-2">{{$janelaFicha['name_field']}}</label>
              <input class="download-modal_form-input w-input" data-name="name" id="name-2" maxlength="256" name="name" required="required" type="text">
            </div>
            <div class="w-col w-col-6">
              <label class="download-modal_form-label" for="email-4">{{$janelaFicha['email_field']}}</label>
              <input class="download-modal_form-input w-input" data-name="email" id="email-4" maxlength="256" name="email" required="required" type="email">
            </div>
          </div>
          <input class="btn download-modal_form-btn w-button" data-wait="enviando..." type="submit" value="{{$janelaFicha['submit_field']}}">
        </form>
        <div class="w-form-done">
          <div>Obrigado! Você se cadastrou para receber novidades!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Algo deu errado</div>
        </div>
      </div>
    </div>
    <a class="navbar-close-button w-inline-block" data-ix="closedownloadmodal" href="#"></a>
  </div>
  <div class="news-modal" id="modal2">
    <div class="container">
      <h2 class="download-modal_title">{{$janelaNews['title']}}</h2>
      <div class="modal-download_paragraph">{!!$janelaNews['description']!!}</div>
      <div class="w-form">
        <form class="w-clearfix form-ajax" action="http://beta.site321.com.br/api/v2/contents/15c3d41d5cc1d280" data-name="Email Form 4" id="email-form-4" name="email-form-4">
          <div class="w-row">
            <div class="w-col w-col-6">
              <label class="download-modal_form-label" for="name-2">{{$janelaNews['name_field']}}</label>
              <input class="download-modal_form-input w-input" data-name="name" id="name-2" maxlength="256" name="name" required="required" type="text">
            </div>
            <div class="w-col w-col-6">
              <label class="download-modal_form-label" for="email-4">{{$janelaNews['email_field']}}</label>
              <input class="download-modal_form-input w-input" data-name="email" id="email-4" maxlength="256" name="email" required="required" type="email">
            </div>
          </div>
          <input class="btn download-modal_form-btn w-button" data-wait="Please wait..." type="submit" value="{{$janelaNews['submit_field']}}">
        </form>
        <div class="w-form-done">
          <div>Obrigado! Você se cadastrou para receber novidades!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Algo deu errado</div>
        </div>
      </div>
    </div>
    <a class="navbar-close-button w-inline-block" id="fechamodal" href="#"></a>
  </div>