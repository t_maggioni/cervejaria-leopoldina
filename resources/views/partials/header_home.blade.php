<section class="hero-section">
    <div class="container">
      <nav class="main-navbar w-clearfix" id="header">
        <a class="main-navbar_logo-box w-inline-block" href="#">
          <img class="main-navbar_logo-image" src="{{ URL::asset('images/logo-leopoldina-svg.svg') }}" width="175">
        </a>
        <div class="main-navbar-menu">
          <div class="main-navbar_icons">
            <a class="main-navbar_submenu-item" id="abremodal" href="#">Receba todas as novidades</a>
            <a class="main-navbar_submenu-item" data-ix="opendownloadmodal" href="#">BAIXE O GUIA DE HARMONIZAÇÃO</a>
            <a class="main-navbar_icons-social" href="{{$tema['facebook']}}" target="_blank"></a>
            <a class="main-navbar_icons-social" href="{{$tema['instagram']}}" target="_blank"></a>
            <div class="main-navbar_icons-divisor"></div>
            <a class="main-navbar_icons-search w-inline-block" data-ix="opensearchmodal" href="#"></a>
          </div>
          <div class="navigation w-nav" data-animation="default" data-collapse="medium" data-duration="400">
            <nav class="navigation-menu w-nav-menu" role="navigation">
              @foreach ($menu as $m )
                <a class="main-navbar-menu_link" target="{{ $m['target'] }}" title="{{ $m['title'] }}" href="{{ $m['url'] }}">{{ $m['label'] }}</a>
              @endforeach
            </nav>
            <div class="navigation-menu-button w-nav-button">
              <div class="w-icon-nav-menu"></div>
            </div>
          </div>
        </div>
      </nav>
      <div class="hero-section_content">
        <h1 class="hero-section_title">{{ $data['title_section_1'] }}</h1>
        <div class="hero-section_paragraph">{!! $data['description_section_1'] !!}</div>
        <a class="btn w-button" href="{{ $data['btn1_url_section_1'] }}">{{ $data['btn1_text_section_1'] }}</a>
        <a class="btn btn_style_2 hero-section_2button w-button" href="{{ $data['btn2_url_section_1'] }}" target="_blank">{{ $data['btn2_text_section_1'] }}</a>
      </div>
    </div>
  </section>
