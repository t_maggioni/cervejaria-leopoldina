<div class="signature-section">
  <div class="container">
    <div class="signature-main-div">
      <div class="w-row">
        <div class="w-col w-col-6 w-col-stack">
          <div class="signature-div">
            <div>
              {{ $tema['copyright'] }}
            </div>
          </div>
        </div>
        <div class="w-clearfix w-col w-col-6 w-col-stack">
          <div class="right signature-div">
            {!! $tema['signature'] !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
