<section class="section-newsletter" id="newsletter">
    <div class="container">
      <h2 class="footer-newsletter_title">{{$tema['title_rodape']}}</h2>
      <div class="w-form">
        <form data-name="Email Form 3" action="http://beta.site321.com.br/api/v2/contents/15c3d41d5cc1d280" class="form-ajax" id="email-form-3" name="email-form-3">
          <input class="footer-newsletter_input w-input" data-name="email" id="email-2" maxlength="256" name="email" placeholder="Seu melhor e-mail" required="required" type="email">
          <input class="footer-newsletter_btn w-button" data-wait="Please wait..." type="submit" value="Assinar">
        </form>
        <div class="w-form-done">
          <div>Obrigado! Você se cadastrou para receber novidades!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Algo deu errado</div>
        </div>
      </div>
    </div>
  </section>
  <footer class="footer" data-ix="showbacktopbutton">
    <a class="back-top-button w-inline-block" href="#header">
      <img src="images/back_top_icon.png" width="25">
    </a>
    <div class="container">
      <div class="w-row">
        <div class="w-col w-col-3">
          <h3 class="footer-h3">Entre em contato.</h3>
          <div class="footer-text">{{$tema['phone']}}</div>
        </div>
        <div class="w-col w-col-5">
          <h3 class="footer-h3">Venha nos visitar.</h3>
          <div class="footer-text">{{$tema['address']}}</div>
        </div>
        <div class="w-col w-col-4">
          <h3 class="footer-h3">{{$tema['item_download']}}</h3>
          <a class="footer-text link" href="{{$tema['item_download_url']}}">Faça o download</a>
          <div class="footer-social-icons-box">
            <a class="footer-social-icon" href="{{$tema['facebook']}}" target="_blank"></a>
            <a class="footer-social-icon" href="{{$tema['instagram']}}" target="_blank"></a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <div class="copyright">
    <div class="container">
      <div class="copyright-text">{{$tema['copyright']}}</div>
      <div class="copyright-text od-text">{!!$tema['signature']!!}</div>
    </div>
  </div>