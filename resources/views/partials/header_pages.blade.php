<header class="header-pages" id="header">
    <div class="container">
      <nav class="main-navbar w-clearfix">
        <a class="main-navbar_logo-box page-header_logo w-inline-block" href="#">
          <img class="main-navbar_logo-image" src="{{ URL::asset('images/logo-leopoldina-svg.svg') }}" width="175">
        </a>
        <div class="main-navbar-menu">
          <div class="main-navbar_icons">
            <a class="main-navbar_submenu-item page-header_submenu-link" id="abremodal" href="#">Receba todas as novidades</a>
            <a class="main-navbar_submenu-item page-header_submenu-link" data-ix="opendownloadmodal" href="#">BAIXE O GUIA DE HARMONIZAÇÃO</a>
            <a class="main-navbar_icons-social page-header_social-icon" href="{{$tema['facebook']}}" target="_blank"></a>
            <a class="main-navbar_icons-social page-header_social-icon" href="{{$tema['instagram']}}" target="_blank"></a>
            <div class="main-navbar_icons-divisor page-header_icons-divisor"></div>
            <a class="main-navbar_icons-search page-header_search-icon w-inline-block" data-ix="opensearchmodal" href="#"></a>
          </div>
          <div class="navigation w-nav" data-animation="default" data-collapse="medium" data-duration="400">
            <nav class="navigation-menu w-nav-menu" role="navigation">
              @foreach ($menu as $m )
                
                <a class="main-navbar-menu_link page-header_navbar-link" target="{{ $m['target'] }}" title="{{ $m['title'] }}" href="{{ $m['url'] }}">{{ $m['label'] }}</a>
              @endforeach
            </nav>
            <div class="header-pages_menu-button navigation-menu-button w-nav-button">
              <div class="w-icon-nav-menu"></div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  </header>
<header class="page-title">
    <img class="page-header-title-image-1" data-ix="bottomanimation-2" src="{{ URL::asset('images/arabesco_title_page.png') }}">
    <h1 class="page-header-title" data-ix="bottomanimation-1">{{$data['title']}}</h1>
    <img class="page-header-title-image-1" data-ix="bottomanimation-2" src="{{ URL::asset('images/arabesco_title_page2.png') }}">
  </header>
