@php
  $groups = collect($resource['cardapio']['items'])->split(2);
  $photo = !empty($resource['photo']) ? Helper::image_url($resource['photo'], '1150x300#', $resource['title']) : '/images/teste.jpg';

  $css = "background-image: -webkit-linear-gradient(270deg, hsla(0, 0%, 100%, 0) 69%, #000), url('$photo');background-image: linear-gradient(180deg, hsla(0, 0%, 100%, 0) 69%, #000), url('$photo');"

@endphp

<div class="w-slide">
  <div class="food-menu-img" style="{{ $css }}">
    <div class="food-title-div">
      @if(!empty($resource['icon']))
        <img class="food-title-main-icon sausage" sizes="39px" src="{{ Helper::image_url($resource['icon'], 'x40', 'icon-'.$resource['title']) }}" width="39"/>
      @endif
      <h2 class="food-content-title_h2">{{ $resource['title'] }}</h2>
    </div>
  </div>
  <div class="food-content-div">
    @foreach($groups as $values)
      <div class="w-col w-col-6">
        @each('elements.resource_menu', $values, 'item')
      </div>
    @endforeach
  </div>
</div>
