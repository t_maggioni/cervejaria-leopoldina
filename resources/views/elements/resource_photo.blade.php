{{-- <div class="img-div" style="background-image: url({{ $photo }})"></div> --}}
<a href="{{ Helper::image_url($photo['url'], null, 'fotos-cafe-colonial-gramado') }}" title="{{$photo['legend']}}" class="photo-content-div">
  <img class="img-div" alt="{{$photo['legend']}}" src="{{ Helper::image_url($photo['url'], '348x348%23', 'thumb-cafe-colonial-gramado') }}" />
</a>
