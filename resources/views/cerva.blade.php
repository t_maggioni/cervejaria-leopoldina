@extends('layouts.app')

@section('header')
@include('partials.header_pages')
@endsection
@section('content')
<div class="page-beer-section section-beer">
    <div class="container">
      <div class="section-beer-header">
        <div class="w-dropdown" data-delay="0" data-hover="1">
          <div class="section-beer-header_more-link w-dropdown-toggle">
            <div>Ver mais produtos</div>
          </div>
          <nav class="section-beer_dropdown-list w-dropdown-list">
              @foreach ($cervejas['items'] as $cerveja)
            <a class="section-beer_dropdown-item w-dropdown-link" href="/cervejas/{{$cerveja['slug']}}">{{$cerveja['title']}}</a>
             @endforeach
            <a class="dropdown-btn-all-items section-beer_dropdown-item w-dropdown-link" href="/cervejas">+ Ver todos os produtos</a>
          </nav>
        </div>
      </div>
      <div class="w-row">
        <div class="beer-page_img-column w-col w-col-3">
          <img data-ix="bottomanimation-1" src="{{ Helper::image_url($cerva['items'][0]['foto_da_garrafa'], '230x') }}" width="230">
          <a class="beer-page_btn btn w-button" href="{{$cerva['items'][0]['btn_acao_1_url']}}">{{$cerva['items'][0]['btn_acao_1_texto']}}</a>
          <a class="beer-page_btn btn btn_style_2 w-button" href="{{$cerva['items'][0]['btn_acao_2_url']}}">{{$cerva['items'][0]['btn_acao_2_texto']}}</a>
          <a class="beer-page_btn btn btn_style_2 w-button" data-ix="opendownloadmodal" href="{{$cerva['items'][0]['btn_acao_3_url']}}">{{$cerva['items'][0]['btn_acao_3_texto']}}</a>
        </div>
        <div class="infos-column w-col w-col-9">
          <div class="section-beer_title-line"></div>
          <h1 class="beer-page-title section-title" data-ix="bottomanimation-2">{{$cerva['items'][0]['title']}}</h1>
          <div class="section-first-beer-paragraph section-paragraph" data-ix="bottomanimation-2">{!! $cerva['items'][0]['descricao']!!}</div>
          <div>
            <div class="w-row">
              <div class="w-col w-col-6">
                <div class="w-row">
                  <div class="details-image-column w-col w-col-2">
                    <img src="{{ URL::asset('images/harmonizacao_icon.png') }}" width="40">
                  </div>
                  <div class="w-col w-col-10">
                    <h1 class="details-item-title">Harmonização</h1>
                    <p class="details-item-paragraph">{{$cerva['items'][0]['harmonizacao']}}</p>
                  </div>
                </div>
              </div>
              <div class="w-col w-col-6">
                <div class="w-row">
                  <div class="details-image-column w-col w-col-2">
                    <img src="{{ URL::asset('images/temperatura_icon.png') }}" width="39">
                  </div>
                  <div class="w-col w-col-10">
                    <h1 class="details-item-title">Consumo</h1>
                    <p class="details-item-paragraph">{{$cerva['items'][0]['consumo']}}</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="w-row">
              <div class="w-col w-col-6">
                <div class="w-row">
                  <div class="details-image-column w-col w-col-2">
                    <img src="{{ URL::asset('images/graduacao_icon.png') }}" width="28">
                  </div>
                  <div class="w-col w-col-10">
                    <h1 class="details-item-title">Graduação Alcoólica</h1>
                    <p class="details-item-paragraph">{{$cerva['items'][0]['graduacao_alcoolica']}}</p>
                  </div>
                </div>
              </div>
              <div class="w-col w-col-6">
                <div class="w-row">
                  <div class="details-image-column w-col w-col-2">
                    <img src="{{ URL::asset('images/amargor_icon.png') }}" width="41">
                  </div>
                  <div class="w-col w-col-10">
                    <h1 class="details-item-title">Amargor</h1>
                    <p class="details-item-paragraph">{{$cerva['items'][0]['amargor']}}</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="w-row">
              <div class="w-col w-col-6">
                <div class="w-row">
                  <div class="details-image-column w-col w-col-2">
                    <img src="{{ URL::asset('images/coloracao_icon.png') }}" width="40">
                  </div>
                  <div class="w-col w-col-10">
                    <h1 class="details-item-title">Coloração</h1>
                    <p class="details-item-paragraph">{{$cerva['items'][0]['coloracao']}}</p>
                  </div>
                </div>
              </div>
              <div class="w-col w-col-6">
                <div class="w-row">
                  <div class="details-image-column w-col w-col-2">
                    <img src="{{ URL::asset('images/copo_icon.png') }}" width="25">
                  </div>
                  <div class="w-col w-col-10">
                    <h1 class="details-item-title">Copo para Degustação</h1>
                    <p class="details-item-paragraph">{{$cerva['items'][0]['copo_para_degustacao']}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="beer-elements_div w-row">
              <div class="w-col w-col-6">
                <div class="elements-title_line section-beer_title-line"></div>
                <h2 class="beer-elements_title">Copo</h2>
                <img src="{{ Helper::image_url($cerva['items'][0]['foto_copo_para_degustacao'], '230x') }}" width="185">
              </div>
              <div class="w-col w-col-6">
                <div class="elements-title_line section-beer_title-line"></div>
                <h2 class="beer-elements_title">Elementos</h2>
                @foreach($elementos['items'] as $elemento)
                <div class="beer-elements_box" style="background-color:{{$elemento['color']['value']}}; border-top:5px solid {{$elemento['color']['value']}}">
                  <div>{{$elemento['title']}}</div>
                </div>
                @endforeach
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
@endsection