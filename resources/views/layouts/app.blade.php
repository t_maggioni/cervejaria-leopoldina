<html data-wf-page="5995ad074ec09b0001561206" data-wf-site="59319758fadfd279bdedf811">
@include('main_partials.head')
<body>
   @include('main_partials.tracking_body')
  <div class="{{ isset($home) ? 'home-main-content' : 'internal-page-body' }}" style="background-image: -webkit-linear-gradient(270deg, rgba(0, 0, 0, .22), rgba(0, 0, 0, .22)), -webkit-linear-gradient(270deg, hsla(0, 0%, 100%, 0) 86%, rgba(0, 0, 0, .79)), url('{{ Helper::image_url($tema['bg_image'], null, 'imagem-de-fundo') }}')">
    @include('main_partials.search_down')
    @yield('header')
   
    @yield('content')
    @include('partials.footer')
  

  @include('main_partials.javascript_footer')

  @yield('javascript')
</body>
</html>

