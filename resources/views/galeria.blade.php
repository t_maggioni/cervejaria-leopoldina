@extends('layouts.app')

@section('header')
@include('partials.header_pages')
@endsection
@section('content')

   <div class="paragraph-bg">
    <div class="container">
      <div class="fabric-paragraph section-paragraph" data-ix="bottomanimation-2">{!! $data['description'] !!}</div>
    </div>
  </div>
 <?php $j = 0; $size = ""; ?>
  <div class="page-content">
        <div class="container_mosaico">

          <!-- <div class="project two_rows">
              <img src="https://www.thumb321.com.br/img-009d7549e94da22d" class="img" /> 
            </div>
           <div class="project two_columns">
              <img src="https://www.thumb321.com.br/img-009d7549e94da22d" class="img" /> 
            </div>
            <div class="project square">
              <img src="https://www.thumb321.com.br/img-009d7549e94da22d" class="img" /> 
            </div>
            <div class="project two_rows">
              <img src="https://www.thumb321.com.br/img-009d7549e94da22d" class="img" /> 
            </div>
            <div class="project two_rows">
              <img src="https://www.thumb321.com.br/img-009d7549e94da22d" class="img" /> 
            </div>
            <div class="project square">
              <img src="https://www.thumb321.com.br/img-009d7549e94da22d" class="img" /> 
            </div>
            <div class="project two_columns">
              <img src="https://www.thumb321.com.br/img-009d7549e94da22d" class="img" /> 
            </div>  -->


    <?php foreach($data['photos'] as $img):

   
   
    if ($j == 0 || $j == 4) {
        $class = 'two_rows ' . $j;
         $size = "600x";
       
      }
      if ($j == 2 || $j == 3  || $j == 5) {
        $class = 'square ' . $j;
        $size = "600x";
       
      }
      else if ($j == 1 || $j == 6) {
        $class = 'two_columns ' . $j;
        $size = "1226x";
        
      }
    
    ?>
  
    <div class="project {{ $class }}">
      <img src="{{ Helper::image_url($img['url'], $size) }}" class="img" /> 
    </div>

   
    
   <?php 
    
    if ($j == 6){
      $j = -1;
    } 
    ?>
    
    <?php $j++; endforeach; ?>
    
  </div>
</div>
@endsection



@section('javascript')
<script src="{{ URL::asset('js/masonry.pkgd.min.js') }}" type="text/javascript"></script>
<script>
  $(document).ready(function(){
  $('.container_mosaico').masonry({
        itemSelector: '.project',
        columnWidth: '.project',
        originLeft:true,
        percentPosition: true
    });
});
</script>
@endsection

<!-- <?php if ($j == 4)
    // {
    ?>
    </div>
    <div class="container_mosaico">-->
